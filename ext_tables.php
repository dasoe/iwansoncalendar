<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
            'Dasoe.Iwansoncalendar',
            'Showcalendar',
            'show calendar'
        );

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
            'Dasoe.Iwansoncalendar',
            'Showauditions',
            'show auditions'
        );

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('iwansoncalendar', 'Configuration/TypoScript', 'iwanson calendar');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_iwansoncalendar_domain_model_event', 'EXT:iwansoncalendar/Resources/Private/Language/locallang_csh_tx_iwansoncalendar_domain_model_event.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_iwansoncalendar_domain_model_event');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_iwansoncalendar_domain_model_place', 'EXT:iwansoncalendar/Resources/Private/Language/locallang_csh_tx_iwansoncalendar_domain_model_place.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_iwansoncalendar_domain_model_place');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_iwansoncalendar_domain_model_category', 'EXT:iwansoncalendar/Resources/Private/Language/locallang_csh_tx_iwansoncalendar_domain_model_category.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_iwansoncalendar_domain_model_category');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_iwansoncalendar_domain_model_day', 'EXT:iwansoncalendar/Resources/Private/Language/locallang_csh_tx_iwansoncalendar_domain_model_day.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_iwansoncalendar_domain_model_day');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_iwansoncalendar_domain_model_city', 'EXT:iwansoncalendar/Resources/Private/Language/locallang_csh_tx_iwansoncalendar_domain_model_city.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_iwansoncalendar_domain_model_city');

    }
);
