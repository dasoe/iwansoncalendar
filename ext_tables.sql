#
# Table structure for table 'tx_iwansoncalendar_domain_model_event'
#
CREATE TABLE tx_iwansoncalendar_domain_model_event (

	title varchar(255) DEFAULT '' NOT NULL,
	subtitle varchar(255) DEFAULT '' NOT NULL,
	description text,
	linkoverride varchar(255) DEFAULT '' NOT NULL,
	eventstartdate int(11) DEFAULT '0' NOT NULL,
	eventstarttime int(11) DEFAULT '0' NOT NULL,
	eventenddate int(11) DEFAULT '0' NOT NULL,
	eventendtime int(11) DEFAULT '0' NOT NULL,
	longdescription text,
        link varchar(255) DEFAULT '' NOT NULL,
	images int(11) unsigned NOT NULL default '0',
	place int(11) unsigned DEFAULT '0',
	category int(11) unsigned DEFAULT '0',

);

#
# Table structure for table 'tx_iwansoncalendar_domain_model_place'
#
CREATE TABLE tx_iwansoncalendar_domain_model_place (

	title varchar(255) DEFAULT '' NOT NULL,
	street varchar(255) DEFAULT '' NOT NULL,
	zip varchar(255) DEFAULT '' NOT NULL,
	description varchar(255) DEFAULT '' NOT NULL,
	longdescription text,
	city int(11) unsigned DEFAULT '0',

);

#
# Table structure for table 'tx_iwansoncalendar_domain_model_category'
#
CREATE TABLE tx_iwansoncalendar_domain_model_category (

	title varchar(255) DEFAULT '' NOT NULL,
	description varchar(255) DEFAULT '' NOT NULL,
	symbol int(11) unsigned NOT NULL default '0',

);

#
# Table structure for table 'tx_iwansoncalendar_domain_model_day'
#
CREATE TABLE tx_iwansoncalendar_domain_model_day (

	unused varchar(255) DEFAULT '' NOT NULL,

);

#
# Table structure for table 'tx_iwansoncalendar_domain_model_city'
#
CREATE TABLE tx_iwansoncalendar_domain_model_city (

	name varchar(255) DEFAULT '' NOT NULL,
	country int(11) unsigned DEFAULT '0',

);

#
# Table structure for table 'static_countries'
#
CREATE TABLE static_countries (

	tx_extbase_type varchar(255) DEFAULT '' NOT NULL,

);
