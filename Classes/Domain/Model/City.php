<?php
namespace Dasoe\Iwansoncalendar\Domain\Model;


/***
 *
 * This file is part of the "iwanson calendar" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2020 das oe <christian.oettinger@gmx.de>
 *
 ***/
/**
 * City
 */
class City extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{

    /**
     * name
     * 
     * @var string
     */
    protected $name = '';

    /**
     * country
     * 
     * @var \Dasoe\Iwansoncalendar\Domain\Model\Country
     */
    protected $country = null;

    /**
     * Returns the name
     * 
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Sets the name
     * 
     * @param string $name
     * @return void
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Returns the country
     * 
     * @return \Dasoe\Iwansoncalendar\Domain\Model\Country $country
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Sets the country
     * 
     * @param \Dasoe\Iwansoncalendar\Domain\Model\Country $country
     * @return void
     */
    public function setCountry(\Dasoe\Iwansoncalendar\Domain\Model\Country $country)
    {
        $this->country = $country;
    }
}
