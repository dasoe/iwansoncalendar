<?php
namespace Dasoe\Iwansoncalendar\Domain\Model;


/***
 *
 * This file is part of the "iwanson calendar" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2020 das oe <christian.oettinger@gmx.de>
 *
 ***/
/**
 * Category
 */
class Category extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{

    /**
     * title
     * 
     * @var string
     */
    protected $title = '';

    /**
     * description
     * 
     * @var string
     */
    protected $description = '';

    /**
     * symbol
     * 
     * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
     * @TYPO3\CMS\Extbase\Annotation\ORM\Cascade("remove")
     */
    protected $symbol = null;

    /**
     * Returns the title
     * 
     * @return string $title
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Sets the title
     * 
     * @param string $title
     * @return void
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Returns the description
     * 
     * @return string $description
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Sets the description
     * 
     * @param string $description
     * @return void
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * Returns the symbol
     * 
     * @return \TYPO3\CMS\Extbase\Domain\Model\FileReference $symbol
     */
    public function getSymbol()
    {
        return $this->symbol;
    }

    /**
     * Sets the symbol
     * 
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $symbol
     * @return void
     */
    public function setSymbol(\TYPO3\CMS\Extbase\Domain\Model\FileReference $symbol)
    {
        $this->symbol = $symbol;
    }
}
