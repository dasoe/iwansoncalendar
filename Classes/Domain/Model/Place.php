<?php
namespace Dasoe\Iwansoncalendar\Domain\Model;


/***
 *
 * This file is part of the "iwanson calendar" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2020 das oe <christian.oettinger@gmx.de>
 *
 ***/
/**
 * Place
 */
class Place extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{

    /**
     * title
     * 
     * @var string
     */
    protected $title = '';

    /**
     * street
     * 
     * @var string
     */
    protected $street = '';

    /**
     * zip
     * 
     * @var string
     */
    protected $zip = '';

    /**
     * description
     * 
     * @var string
     */
    protected $description = '';

    /**
     * longdescription
     * 
     * @var string
     */
    protected $longdescription = '';

    /**
     * city
     * 
     * @var \Dasoe\Iwansoncalendar\Domain\Model\City
     */
    protected $city = null;

    /**
     * Returns the title
     * 
     * @return string $title
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Sets the title
     * 
     * @param string $title
     * @return void
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Returns the street
     * 
     * @return string $street
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * Sets the street
     * 
     * @param string $street
     * @return void
     */
    public function setStreet($street)
    {
        $this->street = $street;
    }

    /**
     * Returns the zip
     * 
     * @return string $zip
     */
    public function getZip()
    {
        return $this->zip;
    }

    /**
     * Sets the zip
     * 
     * @param string $zip
     * @return void
     */
    public function setZip($zip)
    {
        $this->zip = $zip;
    }

    /**
     * Returns the description
     * 
     * @return string $description
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Sets the description
     * 
     * @param string $description
     * @return void
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * Returns the longdescription
     * 
     * @return string $longdescription
     */
    public function getLongdescription()
    {
        return $this->longdescription;
    }

    /**
     * Sets the longdescription
     * 
     * @param string $longdescription
     * @return void
     */
    public function setLongdescription($longdescription)
    {
        $this->longdescription = $longdescription;
    }

    /**
     * Returns the city
     * 
     * @return \Dasoe\Iwansoncalendar\Domain\Model\City $city
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Sets the city
     * 
     * @param \Dasoe\Iwansoncalendar\Domain\Model\City $city
     * @return void
     */
    public function setCity(\Dasoe\Iwansoncalendar\Domain\Model\City $city)
    {
        $this->city = $city;
    }
}
