<?php
namespace Dasoe\Iwansoncalendar\Domain\Model;


/***
 *
 * This file is part of the "iwanson calendar" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2020 das oe <christian.oettinger@gmx.de>
 *
 ***/
/**
 * Day
 */
class Day extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{

    /**
     * unused
     * 
     * @var string
     */
    protected $unused = '';

    /**
     * Returns the unused
     * 
     * @return string $unused
     */
    public function getUnused()
    {
        return $this->unused;
    }

    /**
     * Sets the unused
     * 
     * @param string $unused
     * @return void
     */
    public function setUnused($unused)
    {
        $this->unused = $unused;
    }
}
