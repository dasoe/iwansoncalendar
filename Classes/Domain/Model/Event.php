<?php
namespace Dasoe\Iwansoncalendar\Domain\Model;


/***
 *
 * This file is part of the "iwanson calendar" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2020 das oe <christian.oettinger@gmx.de>
 *
 ***/
/**
 * Event
 */
class Event extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{

    /**
     * title
     * 
     * @var string
     * @TYPO3\CMS\Extbase\Annotation\Validate("NotEmpty")
     */
    protected $title = '';

    /**
     * subtitle
     * 
     * @var string
     */
    protected $subtitle = '';

    /**
     * description
     * 
     * @var string
     */
    protected $description = '';

    /**
     * linkoverride
     * 
     * @var string
     */
    protected $linkoverride = '';

     /**
     * link
     * 
     * @var string
     */
    protected $link = '';

    
    /**
     * eventstartdate
     * 
     * @var \DateTime
     */
    protected $eventstartdate = null;

    /**
     * eventstarttime
     * 
     * @var \DateTime
     */
    protected $eventstarttime = null;

    /**
     * eventenddate
     * 
     * @var \DateTime
     */
    protected $eventenddate = null;

    /**
     * eventendtime
     * 
     * @var \DateTime
     */
    protected $eventendtime = null;

    /**
     * longdescription
     * 
     * @var string
     */
    protected $longdescription = '';

    /**
     * images
     * 
     * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
     * @TYPO3\CMS\Extbase\Annotation\ORM\Cascade("remove")
     */
    protected $images = null;

    /**
     * place
     * 
     * @var \Dasoe\Iwansoncalendar\Domain\Model\Place
     */
    protected $place = null;

    /**
     * category
     * 
     * @var \Dasoe\Iwansoncalendar\Domain\Model\Category
     */
    protected $category = null;

    /**
     * Returns the title
     * 
     * @return string $title
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Sets the title
     * 
     * @param string $title
     * @return void
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Returns the place
     * 
     * @return \Dasoe\Iwansoncalendar\Domain\Model\Place $place
     */
    public function getPlace()
    {
        return $this->place;
    }

    /**
     * Sets the place
     * 
     * @param \Dasoe\Iwansoncalendar\Domain\Model\Place $place
     * @return void
     */
    public function setPlace(\Dasoe\Iwansoncalendar\Domain\Model\Place $place)
    {
        $this->place = $place;
    }

    /**
     * Returns the category
     * 
     * @return \Dasoe\Iwansoncalendar\Domain\Model\Category $category
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Sets the category
     * 
     * @param \Dasoe\Iwansoncalendar\Domain\Model\Category $category
     * @return void
     */
    public function setCategory(\Dasoe\Iwansoncalendar\Domain\Model\Category $category)
    {
        $this->category = $category;
    }

    /**
     * Returns the subtitle
     * 
     * @return string $subtitle
     */
    public function getSubtitle()
    {
        return $this->subtitle;
    }

    /**
     * Sets the subtitle
     * 
     * @param string $subtitle
     * @return void
     */
    public function setSubtitle($subtitle)
    {
        $this->subtitle = $subtitle;
    }

    /**
     * Returns the description
     * 
     * @return string $description
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Sets the description
     * 
     * @param string $description
     * @return void
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * Returns the linkoverride
     * 
     * @return string $linkoverride
     */
    public function getLinkoverride()
    {
        return $this->linkoverride;
    }

    /**
     * Sets the linkoverride
     * 
     * @param string $linkoverride
     * @return void
     */
    public function setLinkoverride($linkoverride)
    {
        $this->linkoverride = $linkoverride;
    }

    /**
     * Returns the eventstartdate
     * 
     * @return \DateTime $eventstartdate
     */
    public function getEventstartdate()
    {
        return $this->eventstartdate;
    }

    /**
     * Sets the eventstartdate
     * 
     * @param \DateTime $eventstartdate
     * @return void
     */
    public function setEventstartdate(\DateTime $eventstartdate)
    {
        $this->eventstartdate = $eventstartdate;
    }

    /**
     * Returns the eventstarttime
     * 
     * @return \DateTime $eventstarttime
     */
    public function getEventstarttime()
    {
        return $this->eventstarttime;
    }

    /**
     * Sets the eventstarttime
     * 
     * @param \DateTime $eventstarttime
     * @return void
     */
    public function setEventstarttime(\DateTime $eventstarttime)
    {
        $this->eventstarttime = $eventstarttime;
    }
    
    
    /**
     * Returns the eventenddate
     * 
     * @return \DateTime $eventenddate
     */
    public function getEventenddate()
    {
        return $this->eventenddate;
    }

    /**
     * Sets the eventenddate
     * 
     * @param \DateTime $eventenddate
     * @return void
     */
    public function setEventenddate(\DateTime $eventenddate)
    {
        $this->eventenddate = $eventenddate;
    }

    /**
     * Returns the eventendtime
     * 
     * @return \DateTime $eventendtime
     */
    public function getEventendtime()
    {
        return $this->eventendtime;
    }

    /**
     * Sets the eventendtime
     * 
     * @param \DateTime $eventendtime
     * @return void
     */
    public function setEventendtime(\DateTime $eventendtime)
    {
        $this->eventendtime = $eventendtime;
    }

    /**
     * Returns the longdescription
     * 
     * @return string $longdescription
     */
    public function getLongdescription()
    {
        return $this->longdescription;
    }

    /**
     * Sets the longdescription
     * 
     * @param string $longdescription
     * @return void
     */
    public function setLongdescription($longdescription)
    {
        $this->longdescription = $longdescription;
    }

    /**
     * Returns the link
     * 
     * @return string $link
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * Sets the link
     * 
     * @param string $link
     * @return void
     */
    public function setLink($link)
    {
        $this->link = $link;
    }
    
    /**
     * Returns the images
     * 
     * @return \TYPO3\CMS\Extbase\Domain\Model\FileReference $images
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * Sets the images
     * 
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $images
     * @return void
     */
    public function setImages(\TYPO3\CMS\Extbase\Domain\Model\FileReference $images)
    {
        $this->images = $images;
    }
}
