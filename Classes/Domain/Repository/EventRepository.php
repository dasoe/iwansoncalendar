<?php
namespace Dasoe\Iwansoncalendar\Domain\Repository;

use TYPO3\CMS\Extbase\Utility\DebuggerUtility;
/***
 *
 * This file is part of the "iwanson calendar" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2020 das oe <christian.oettinger@gmx.de>
 *
 ***/
/**
 * The repository for Events
 */
class EventRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{
        public function findAllActualMonth() {
        $firstDate = new \DateTime('first day of this month');
        $lastDate = new \DateTime('last day of this month');
        $firstDate->setTime(0, 0, 0);
        $lastDate->setTime(24, 0, 0);
 
        $query = $this->createQuery();
        $query->setOrderings(
                [
                    'eventstartdate' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING
                ]
        );
        $query->matching(
                $query->logicalOR(
                        [
                            $query->logicalAND(
                                [
                                   $query->greaterThanOrEqual('eventstartdate', $firstDate),
                                   $query->lessThanOrEqual('eventstartdate', $lastDate),                            
                                ]),
                           $query->logicalAND(
                                [
                                   $query->greaterThanOrEqual('eventenddate', $firstDate),
                                   $query->lessThanOrEqual('eventenddate', $lastDate),                            
                                ])                            
                        ]
                )
        );
        return $query->execute();
    }
    
    
    /**
     * findAllForMonth
     * 
     * @param int $actualMonth
     * @param int $actualYear
     */
    public function findAllForMonth($actualMonth, $actualYear) {
        
        $query_date = "$actualYear-$actualMonth-01";
        $firstDate = new \DateTime( date('Y-m-01', strtotime($query_date)) );
        $lastDate = new \DateTime( date('Y-m-t', strtotime($query_date)) );
        $firstDate->setTime(0, 0, 0);
        $lastDate->setTime(24, 0, 0);
 
  
        $query = $this->createQuery();
        $query->setOrderings(
                [
                    'eventstartdate' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING
                ]
        );
        $query->matching(
                $query->logicalOR(
                        [
                            $query->logicalAND(
                                [
                                   $query->greaterThanOrEqual('eventstartdate', $firstDate),
                                   $query->lessThanOrEqual('eventstartdate', $lastDate),                            
                                ]),
                           $query->logicalAND(
                                [
                                   $query->greaterThanOrEqual('eventenddate', $firstDate),
                                   $query->lessThanOrEqual('eventenddate', $lastDate),                            
                                ])                            
                        ]
                )
        );
        return $query->execute();
    }    
    
}
