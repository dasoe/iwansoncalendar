<?php
namespace Dasoe\Iwansoncalendar\Controller;

use TYPO3\CMS\Extbase\Utility\DebuggerUtility;

/***
 *
 * This file is part of the "iwanson calendar" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2020 das oe <christian.oettinger@gmx.de>
 *
 ***/
/**
 * EventController
 */
class EventController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{

    /**
     * eventRepository
     * 
     * @var \Dasoe\Iwansoncalendar\Domain\Repository\EventRepository
     * @inject
     */
    protected $eventRepository = null;

        /**
     * categoryRepository
     * 
     * @var \Dasoe\Iwansoncalendar\Domain\Repository\CategoryRepository
     * @inject
     */
    protected $categoryRepository = null;

    /**
     * action list
     * 
     * @return void
     */
    public function listAction()
    {
        $events = $this->eventRepository->findAllActualMonth();
        $categories = $this->categoryRepository->findAll();
        $prepEvents = Array();
        $today = getdate();
        // DebuggerUtility::var_dump( $events );
        foreach ($events as $event) {
            // many-day event 
            if (  $event->getEventenddate() && $event->getEventstartdate()->format("Y-d") != $event->getEventenddate()->format("Y-d") ) {
                // TODO Montaspass (lastdate day kleiner als firstdate day!
                if ( intval($event->getEventenddate()->format("d")) > intval($event->getEventstartdate()->format("d")) ) {

                    for ( $i = intval($event->getEventstartdate()->format("d")); $i <= intval($event->getEventenddate()->format("d")); $i++ ) {
              //DebuggerUtility::var_dump( $i );
                         $prepEvents[ $today["year"] . "-" . sprintf('%02d', $today["mon"]) . "-" .sprintf('%02d',  $i) ][] = $event;
                    }
                    
                    
                } else {
                // days span month change
                    //DebuggerUtility::var_dump( $event );
                }
            } else {
            // single day event
              $prepEvents[ $today["year"] . "-" . sprintf('%02d', $today["mon"]) . "-" . $event->getEventstartdate()->format("d") ][] = $event;
            }
                           
          //  DebuggerUtility::var_dump(intval($event->getEventstartdate()->format("d")));         

        }
         //DebuggerUtility::var_dump( $prepEvents );
        
        $this->view->assign( 'dateFormatString', "%d.%m.%y" );

        $this->view->assign( 'actualMonth', $today["mon"] );
        $this->view->assign( 'actualMonthText', strftime( "%B" )  );
        
        $this->view->assign( 'actualYear', $today["year"] );
        $this->view->assign('categories', $categories);
        $this->view->assign('events', $prepEvents);
    }

    /**
     * action list
     * 
     * @param int $actualMonth
     * @param int $actualYear
     */
    public function listMonthAction(int $actualMonth, int $actualYear)
    {
        //DebuggerUtility::var_dump( $actualMonth );

        $events = $this->eventRepository->findAllForMonth($actualMonth, $actualYear);
        $categories = $this->categoryRepository->findAll();
        
        $prepEvents = Array();
        foreach ($events as $event) {
            $prepEvents[$event->getEventstartdate()->format("Y-m-d")][] = $event;
        }
                 //DebuggerUtility::var_dump( $prepEvents );

        $this->view->assign( 'actualMonth', $actualMonth );
        $this->view->assign( 'actualMonthText', strftime( "%B", strtotime( $actualMonth."/1/".$actualYear ) )  );        
        $this->view->assign( 'actualYear', $actualYear );
        $this->view->assign('categories', $categories);
        $this->view->assign('events', $prepEvents);
    }    
    
   
    
    /**
     * action show
     * 
     * @param \Dasoe\Iwansoncalendar\Domain\Model\Event $event
     * @return void
     */
    public function showAction(\Dasoe\Iwansoncalendar\Domain\Model\Event $event)
    {
        $this->view->assign('event', $event);
    }
}
