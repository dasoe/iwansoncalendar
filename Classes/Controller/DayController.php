<?php
namespace Dasoe\Iwansoncalendar\Controller;


/***
 *
 * This file is part of the "iwanson calendar" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2020 das oe <christian.oettinger@gmx.de>
 *
 ***/
/**
 * DayController
 */
class DayController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{

    /**
     * action list
     * 
     * @return void
     */
    public function listAction()
    {
        $days = $this->dayRepository->findAll();
        $this->view->assign('days', $days);
    }

    /**
     * action show
     * 
     * @param \Dasoe\Iwansoncalendar\Domain\Model\Day $day
     * @return void
     */
    public function showAction(\Dasoe\Iwansoncalendar\Domain\Model\Day $day)
    {
        $this->view->assign('day', $day);
    }
}
