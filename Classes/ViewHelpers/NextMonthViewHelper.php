<?php

namespace Dasoe\Iwansoncalendar\ViewHelpers;

use TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface;
use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;
/**
 */

/**
 * Class IsPassedViewHelper
 */
    
class NextMonthViewHelper extends AbstractViewHelper
{

    public function initializeArguments()
{
    $this->registerArgument('month', 'integer', 'well, the month', true);
    $this->registerArgument('year', 'integer', 'well, the year', true);
    $this->registerArgument('out', 'string', 'events', true);
//    $this->registerArgument('enddate', 'string', 'the end date', true);
    
}
    
   public static function renderStatic(
       array $arguments,
       \Closure $renderChildrenClosure,
       RenderingContextInterface $renderingContext
   ) {           
       
        $arguments['month']++;
        if ($arguments['month']>12) {
            $arguments['month'] = 1;
            $arguments['year']++;
        }
        
        if ( $arguments['out'] == 'month' ) {
            $output = $arguments['month'];
        } else if ( $arguments['out'] == 'year' ) {
            $output = $arguments['year'];
        }
       if ($output) {
            return $output;
       }
       return -9999;
   }
   
}

