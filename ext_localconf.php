<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'Dasoe.Iwansoncalendar',
            'Showcalendar',
            [
                'Event' => 'list, listMonth, show',
                'Category' => 'list',
                'Day' => 'list, show'
            ],
            // non-cacheable actions
            [
                'Event' => '',
                'Category' => '',
                'Day' => ''
            ]
        );

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'Dasoe.Iwansoncalendar',
            'Showauditions',
            [
                'Event' => 'list, show',
                'Category' => 'list',
                'Day' => 'list, show'
            ],
            // non-cacheable actions
            [
                'Event' => '',
                'Category' => '',
                'Day' => ''
            ]
        );

    // wizards
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
        'mod {
            wizards.newContentElement.wizardItems.plugins {
                elements {
                    showcalendar {
                        iconIdentifier = iwansoncalendar-plugin-showcalendar
                        title = LLL:EXT:iwansoncalendar/Resources/Private/Language/locallang_db.xlf:tx_iwansoncalendar_showcalendar.name
                        description = LLL:EXT:iwansoncalendar/Resources/Private/Language/locallang_db.xlf:tx_iwansoncalendar_showcalendar.description
                        tt_content_defValues {
                            CType = list
                            list_type = iwansoncalendar_showcalendar
                        }
                    }
                    showauditions {
                        iconIdentifier = iwansoncalendar-plugin-showauditions
                        title = LLL:EXT:iwansoncalendar/Resources/Private/Language/locallang_db.xlf:tx_iwansoncalendar_showauditions.name
                        description = LLL:EXT:iwansoncalendar/Resources/Private/Language/locallang_db.xlf:tx_iwansoncalendar_showauditions.description
                        tt_content_defValues {
                            CType = list
                            list_type = iwansoncalendar_showauditions
                        }
                    }
                }
                show = *
            }
       }'
    );
		$iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class);
		
			$iconRegistry->registerIcon(
				'iwansoncalendar-plugin-showcalendar',
				\TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
				['source' => 'EXT:iwansoncalendar/Resources/Public/Icons/user_plugin_showcalendar.svg']
			);
		
			$iconRegistry->registerIcon(
				'iwansoncalendar-plugin-showauditions',
				\TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
				['source' => 'EXT:iwansoncalendar/Resources/Public/Icons/user_plugin_showauditions.svg']
			);
		
    }
);
