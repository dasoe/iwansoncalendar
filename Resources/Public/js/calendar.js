/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var openPleaseAdjust = false;

jQuery.fn.animateAuto = function (prop, speed, callback) {
    var elem, height, width;
    return this.each(function (i, el) {
        el = jQuery(el), elem = el.clone().css({"height": "auto", "width": "auto"}).appendTo("body");


        height = elem.innerHeight();
        width = elem.css("width");
        console.log("I read " + width);
        console.log("x I read " + height);

        elem.remove();

        if (prop === "height")
            el.velocity({"height": height}, speed, callback);
        else if (prop === "width")
            el.animate({"width": width}, speed, callback);
        else if (prop === "both")
            el.animate({"width": width, "height": height}, speed, callback);
    });
}

$(document).ready(function () {
    console.log("ready!");
    console.log("env: " + env);

    $('.daynumber').each(function (index, el) {
        if ( $(this).find('img').length !== 0)  {
            $(this).addClass("hasEvent");
            $(this).parent().addClass("hasEvent");
        }
    });

    if (env == "desktop") {
        $('div.daynumber.hasEvent').on("mouseover", function (event, data) {
            console.log("mouse smaller");
            $('#mouseReplacement').css({
                'background-size': '35%'
            });
        });
        $('div.daynumber.hasEvent').on("mouseout", function (event, data) {
            $('#mouseReplacement').css({
                'background-size': '100%'
            });
        });
        $('.legende .wrapper').on("mouseover", function (event, data) {
            console.log("mouse smaller");
            $('#mouseReplacement').css({
                'background-size': '35%'
            });
        });
        $('.legende .wrapper').on("mouseout", function (event, data) {
            $('#mouseReplacement').css({
                'background-size': '100%'
            });
        });        
    }



    $('.oeCal.day').on("click", function (event, data) {
        if (!$(this).hasClass("open") && $(this).hasClass("hasEvent") ) {
            console.log("Clicked on day...");

            let clickedDayObj = $(this);
            let clickedDay = $(this).attr("id");
            showDay(clickedDay);
            //console.log("clicked a day (" + clickedDay + "). Scroll to: " + ( $(this).offset().top - (oUnt * 2)) );

            if (env == "mobile") {
                console.log("mobile...");

                $('.oeCal.day').removeClass("open");
                $(this).addClass("open");
                $(this).after($('.calEventsWrapper'));
                $('.calEvents').css({
                    height: 'auto',
                });

                console.log($('.calEvents').height());
                $(".calEventsWrapper").velocity({
                    'height': $('.calEvents').height(),
                });

            } else {
                console.log("no mobile...");

                $('.oeCal.day').removeClass("open");
                $(this).addClass("open");
                let contentBody = oUnt*48 ; 
                if (env == "tablet") {
                    contentBody = oUnt*25 ;                    
                } 
                $('.calEventsWrapper').velocity({
                    width: contentBody/100*48,
                }, 900, "easeInOutSine");
                let tRight = -oUnt*2;
                if (env == "tablet") {
                    tRight = oUnt;
                } 

                $('.calEvents').velocity({
                    width: '48%',
                    right: tRight,
                }, 900, "easeInOutSine", function () {
                    // complete
                    adjustAllCalendar();
                    $('#calCloseButton').css({
                        'display':'block'
                        
                    });
                    openPleaseAdjust = true;
                    $('h1.calheader').velocity({
                        opacity: 0,
                    }, 400, "easeInOutSine");
                
                });
                $('.calUpperPart').velocity({
                    width: '68%',
                }, 900, "easeInOutSine");

                $('.calendarFullMonthWrapper').velocity({
                    width: '52%',
                }, 900, "easeInOutSine");

                $('.calUpperPartSteuerung.right h1').velocity({
                    opacity: '0',
                }, 400, "easeInOutSine");
            }

            $([document.documentElement, document.body]).animate({
                scrollTop: ( $(this).offset().top - (oUnt * 5))
            }, 1400);
        } else {
            console.log("already open. Not doing anything...");
        }

    });

    $('#calCloseButton').on("click", function (event, data) {
        console.log("Clicked on close Button...");
        openPleaseAdjust = false;
        if (env != 'mobile') {
            $('h1.calheader').velocity({
                opacity: 1,
            }, 400, "easeInOutSine");
        }


        $(this).css({
            display: 'none',
        });

        $('.calEventsWrapper').velocity({
            width: '0',
        }, 900, "easeInOutSine");
        let tRight = -oUnt*2;
        if (env == "tablet") {
            tRight = 0;
        }       
        $('.calEvents').velocity({
            width: '0',
            right: tRight,
        }, 900, "easeInOutSine", function () {
            // complete
        });
        $('.calUpperPart').velocity({
            width: '100%',
        }, 900, "easeInOutSine");

        $('.calendarFullMonthWrapper').velocity({
            width: '100%',
        }, 900, "easeInOutSine");

        $('.calUpperPartSteuerung.right h1').velocity({
        }, 500, "easeInOutSine", function () {
            // complete
            $('.calUpperPartSteuerung.right h1').velocity({
                opacity: '1',
            }, 400, "easeInOutSine");
        });

    });

    $(window).on('scroll', function () {
        if (env != "mobile") {
            // adjust the height in case it is not mobile (where height is adjusted by click)
            $('.calEventsWrapper').css({
                'height': $.documentHeight()
            });
        } else {
            adjustSteuerung();
        }
    });


    $('.oneEvent').on("click", function (event, data) {
        if (env != "mobile") {
            console.log("Clicked on Event...");

            let uid = $(this).data("uidnr");
            $('.oneEvent').removeClass("active");
            $(this).addClass("active");

            let appropDay = $(this).parent();
            console.log(appropDay);

            appropDay.find('.detailContainer').html($('.details.uidnr' + uid).html());

            console.log('DONE');


//        PUT TABLE LOWER CORNER
            let wrapHeight = $('.calEvents').height(),
            eleme1 = appropDay.find('.detailContainer table.details').get(0),
            elemRect ;
            if (eleme1) {
                elemRect = eleme1.getBoundingClientRect(),
                tabM = wrapHeight - elemRect.bottom;
                console.log('wrapHeight: ' + wrapHeight);
                console.log('elemRect.bottom: ' + elemRect.bottom);

                console.log('tabM: ' + tabM);

                appropDay.find('.detailContainer table.details').css({
                    'margin-top': tabM - oUnt * 2,
                });
            }


        }
    });


    // now get all the transformations done
    adjustAllCalendar();
    // do the same in case window is resized
    $(window).resize(function () {
        adjustAllCalendar();
    });
    
    // last adjustments
    if (env == "mobile") {
        $('.calEventsWrapper').append( $('.calEvents') );    
       // console.log("adjustments for mobile first load.")
        $('.daynumber span.number').each(function (index, el) {
            $(this).html( $(this).data("mobileday") );           
        });
        adjustSteuerung();
    }    


    $('.legende .wrapper').on("click", function (event, data) {
        if ( $(this).hasClass("active") ) {
            
            $('img.catSymbol').each(function (i, el) {
                $(this).css({
                    'filter': 'none',
                    '-webkit-filter':  'none'
                });
            });
            $(this).removeClass("active");
            $('.legende .wrapper').removeClass("active");
            
        } else {
            let actCat = $(this).data("category");
             $('.legende .wrapper').removeClass("active");
           $(this).addClass("active");
            console.log("category " + actCat + " chosen");
            $('img.catSymbol').each(function (i, el) {
                //console.log($(this).data("category")  );
                if ( $(this).data("category") ==  actCat) {
                    $(this).css({
                        'filter': 'none',
                        '-webkit-filter':  'none'
                    });
                } else {
                    $(this).css({
                        'filter': 'filter: grayscale(100%) brightness(70%)',
                        '-webkit-filter':  'grayscale(100%) brightness(70%)'
                    });
                }
            });
        }
    });


});

function showDay(day) {
    $(".oneDay").hide();
    $(".oneDay." + day).show();
    $(".oneDay." + day).find(".oneEvent:first").click();
    
}

function adjustSteuerung() {
        //console.log($.documentHeight());
        if ( $(document).scrollTop() > 10  ) {
            if ( !$( '.calUpperPartSteuerung .steuerung .thedate' ).hasClass( "hidden" ) ) {
                console.log( "hide ist" );
                $( '.calUpperPartSteuerung .steuerung .thedate' ).addClass( "hidden" );                    
                $( '.calUpperPartSteuerung .steuerung').css({
                    'top': (window.innerHeight /2 ),
                });
            }
        } else {
            if ( $( '.calUpperPartSteuerung .steuerung .thedate' ).hasClass( "hidden" ) ) {                
                console.log( "show ist" );
                $( '.calUpperPartSteuerung .steuerung .thedate' ).removeClass( "hidden" );                    
                $( '.calUpperPartSteuerung .steuerung').css({
                    'top': oUnt * 2.5,
                });                    
            }
        }
}

function adjustAllCalendar() {
    console.log("adjust All Calendar");


    if (env == "desktop") {
// ###############################################################################################################################
// ###  DESKTOP  #################################################################################################################
// ###############################################################################################################################

        // columns in Content are calculated by flex
        // get size of one Unity ( one quad in grid )
        // using columns calculated by flex as a basis
//        let oUnt = $('#wrap').width() / 48;
//        let onePixel = oUnt / 40;

        $('.oeCal.day').css({
            'margin-right': oUnt / 2,
            'width': oUnt * 2,
            'height': oUnt * 6,
        });
        let rightMarg = window.innerWidth - (oUnt*48 );
        //console.log(rightMarg);
        $('.calEventsWrapper').css({
            'right':  rightMarg,
        });
        $('#calCloseButton').css({
            'left': oUnt * 41.5,
        });

        $('.calEvents').css({
            'top': -oUnt * 7,
            'right': -oUnt * 2,
        }); 

        $('.tx_iwansoncalendar .legende').css({
            'font-size': oPx * 22,
        });
        $('.calUpperPart .legende img').css({
            'width': oPx * 60,
        }); 
        $('.tx_iwansoncalendar .legende .wrapper .text').css({
            'padding-right': oPx * 24,
            'padding-left': oPx * 6,
        }); 
        
        
        let wrapw = $('#wrap').width();
        $('.oneDay').css({
            'width': wrapw / 2.75,
            'padding': oUnt,
        });
        $('.oneDay.day').css({
            'padding-bottom': oUnt * 2,
        });

        if (openPleaseAdjust) {
            
            let contentBody = oUnt*48 ;
            $('.calEventsWrapper').css({
                width: contentBody/100*48,
            });
        }

//        if ( window.innerWidth > 1920 ) {
//            let adjustRight = window.innerWidth - $('#innerWrap').width() -300;
//            console.log( 'adjustRight: ' + adjustRight );
//            $('.calEventsWrapper').css({
//                'right': adjustRight/2
//            });
//        }


        ////      
//      
//      
//      
//      
//        $('.calEvents').css({
//            'width': '100%',
//            'height': '100%'
//        });
    } else if (env == "tablet") {
        
        if (openPleaseAdjust) {
            
            let contentBody = oUnt*25 ;
            $('.calEventsWrapper').css({
                width: contentBody/100*48,
            });
        }
        
            $('.calEvents').css({
                'top': -oUnt * 7,
            }); 
        
        $('div.tx-iwansoncalendar').css({
            'top': - (oUnt * 2),
        });        
        
        $('#calCloseButton').css({
            'left': oUnt * 21,
        });        

        $('.tx_iwansoncalendar .legende').css({
            'font-size': oPx * 22,
        });
        
    } else {
        console.log("adjust mobile");
        $( '.calUpperPartSteuerung .steuerung').css({
            'right': oUnt /10 * 3,
            'left': oUnt /10 * 3,
        });

        $( '.calUpperPartSteuerung .steuerung').css({
            'top': oUnt * 2.5,
        });
        $( '.calUpperPart').css({
            'top': oUnt *1.5,
        });
        
        $('.tx_iwansoncalendar .legende').css({
            'font-size': oPx * 14,
        });    
        
        $('.calUpperPart .legende img').css({
            'width': oPx * 30,
        });         
        $('.tx_iwansoncalendar .legende .wrapper .text').css({
            'padding-left': oPx * 6,
        });            
        
    }


}