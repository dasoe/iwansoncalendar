<?php
defined('TYPO3_MODE') || die();

if (!isset($GLOBALS['TCA']['static_countries']['ctrl']['type'])) {
    // no type field defined, so we define it here. This will only happen the first time the extension is installed!!
    $GLOBALS['TCA']['static_countries']['ctrl']['type'] = 'tx_extbase_type';
    $tempColumnstx_iwansoncalendar_static_countries = [];
    $tempColumnstx_iwansoncalendar_static_countries[$GLOBALS['TCA']['static_countries']['ctrl']['type']] = [
        'exclude' => true,
        'label'   => 'LLL:EXT:iwansoncalendar/Resources/Private/Language/locallang_db.xlf:tx_iwansoncalendar.tx_extbase_type',
        'config' => [
            'type' => 'select',
            'renderType' => 'selectSingle',
            'items' => [
                ['',''],
                ['Country','Tx_Iwansoncalendar_Country']
            ],
            'default' => 'Tx_Iwansoncalendar_Country',
            'size' => 1,
            'maxitems' => 1,
        ]
    ];
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('static_countries', $tempColumnstx_iwansoncalendar_static_countries);
}

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes(
    'static_countries',
    $GLOBALS['TCA']['static_countries']['ctrl']['type'],
    '',
    'after:' . $GLOBALS['TCA']['static_countries']['ctrl']['label']
);

/* inherit and extend the show items from the parent class */

if (isset($GLOBALS['TCA']['static_countries']['types']['1']['showitem'])) {
    $GLOBALS['TCA']['static_countries']['types']['Tx_Iwansoncalendar_Country']['showitem'] = $GLOBALS['TCA']['static_countries']['types']['1']['showitem'];
} elseif(is_array($GLOBALS['TCA']['static_countries']['types'])) {
    // use first entry in types array
    $static_countries_type_definition = reset($GLOBALS['TCA']['static_countries']['types']);
    $GLOBALS['TCA']['static_countries']['types']['Tx_Iwansoncalendar_Country']['showitem'] = $static_countries_type_definition['showitem'];
} else {
    $GLOBALS['TCA']['static_countries']['types']['Tx_Iwansoncalendar_Country']['showitem'] = '';
}
$GLOBALS['TCA']['static_countries']['types']['Tx_Iwansoncalendar_Country']['showitem'] .= ',--div--;LLL:EXT:iwansoncalendar/Resources/Private/Language/locallang_db.xlf:tx_iwansoncalendar_domain_model_country,';
$GLOBALS['TCA']['static_countries']['types']['Tx_Iwansoncalendar_Country']['showitem'] .= '';

$GLOBALS['TCA']['static_countries']['columns'][$GLOBALS['TCA']['static_countries']['ctrl']['type']]['config']['items'][] = ['LLL:EXT:iwansoncalendar/Resources/Private/Language/locallang_db.xlf:static_countries.tx_extbase_type.Tx_Iwansoncalendar_Country','Tx_Iwansoncalendar_Country'];
