<?php
return [
    'ctrl' => [
        'title' => 'LLL:EXT:iwansoncalendar/Resources/Private/Language/locallang_db.xlf:tx_iwansoncalendar_domain_model_event',
        'label' => 'eventstartdate',
        'label_alt' => 'title',
        'label_alt_force' => '1',
        'default_sortby' => 'eventstartdate DESC',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'versioningWS' => true,
        'languageField' => 'sys_language_uid',
        'transOrigPointerField' => 'l10n_parent',
        'transOrigDiffSourceField' => 'l10n_diffsource',
        'delete' => 'deleted',
        'enablecolumns' => [
            'disabled' => 'hidden',
            'starttime' => 'starttime',
            'endtime' => 'endtime',
        ],
        'searchFields' => 'title,subtitle,description,linkoverride,longdescription',
        'iconfile' => 'EXT:iwansoncalendar/Resources/Public/Icons/tx_iwansoncalendar_domain_model_event.gif'
    ],
    'interface' => [
        'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, title, subtitle, eventstartdate, eventenddate, eventendtime, longdescription,link, images, place, category',
    ],
    'types' => [
        '1' => ['showitem' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, --palette--;Daten (Event findet statt am...);datesp, title, subtitle, longdescription,link, images, --palette--;Einsortieren;catsp, --div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.access, starttime, endtime'],
    ],
    'palettes' => [
        'datesp' => [
            'showitem' => '
                eventstartdate, eventstarttime, eventenddate, eventendtime
            ',
        ],   
        'catsp' => [
            'showitem' => '
                place, category
            ',
        ],           
    ],      
    'columns' => [
        'sys_language_uid' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.language',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'special' => 'languages',
                'items' => [
                    [
                        'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.allLanguages',
                        -1,
                        'flags-multiple'
                    ]
                ],
                'default' => 0,
            ],
        ],
        'l10n_parent' => [
            'displayCond' => 'FIELD:sys_language_uid:>:0',
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.l18n_parent',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'default' => 0,
                'items' => [
                    ['', 0],
                ],
                'foreign_table' => 'tx_iwansoncalendar_domain_model_event',
                'foreign_table_where' => 'AND {#tx_iwansoncalendar_domain_model_event}.{#pid}=###CURRENT_PID### AND {#tx_iwansoncalendar_domain_model_event}.{#sys_language_uid} IN (-1,0)',
            ],
        ],
        'l10n_diffsource' => [
            'config' => [
                'type' => 'passthrough',
            ],
        ],
        't3ver_label' => [
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.versionLabel',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'max' => 255,
            ],
        ],
        'hidden' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.visible',
            'config' => [
                'type' => 'check',
                'renderType' => 'checkboxToggle',
                'items' => [
                    [
                        0 => '',
                        1 => '',
                        'invertStateDisplay' => true
                    ]
                ],
            ],
        ],
        'starttime' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.starttime',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'eval' => 'datetime,int',
                'default' => 0,
                'behaviour' => [
                    'allowLanguageSynchronization' => true
                ]
            ],
        ],
        'endtime' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.endtime',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'eval' => 'datetime,int',
                'default' => 0,
                'range' => [
                    'upper' => mktime(0, 0, 0, 1, 1, 2038)
                ],
                'behaviour' => [
                    'allowLanguageSynchronization' => true
                ]
            ],
        ],

        'title' => [
            'exclude' => false,
            'label' => 'LLL:EXT:iwansoncalendar/Resources/Private/Language/locallang_db.xlf:tx_iwansoncalendar_domain_model_event.title',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim,required'
            ],
        ],
        'subtitle' => [
            'exclude' => true,
            'label' => 'LLL:EXT:iwansoncalendar/Resources/Private/Language/locallang_db.xlf:tx_iwansoncalendar_domain_model_event.subtitle',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'description' => [
            'exclude' => false,
            'label' => 'LLL:EXT:iwansoncalendar/Resources/Private/Language/locallang_db.xlf:tx_iwansoncalendar_domain_model_event.description',
            'config' => [
                'type' => 'text',
                'cols' => 40,
                'rows' => 15,
                'eval' => 'trim'
            ]
        ],
        'linkoverride' => [
            'exclude' => true,
            'label' => 'LLL:EXT:iwansoncalendar/Resources/Private/Language/locallang_db.xlf:tx_iwansoncalendar_domain_model_event.linkoverride',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ], 
        'link' => [
            'exclude' => true,
            'label' => 'LLL:EXT:iwansoncalendar/Resources/Private/Language/locallang_db.xlf:tx_iwansoncalendar_domain_model_event.link',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputLink',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'eventstartdate' => [
            'exclude' => false,
            'label' => 'LLL:EXT:iwansoncalendar/Resources/Private/Language/locallang_db.xlf:tx_iwansoncalendar_domain_model_event.eventstartdate',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'size' => 10,
                'eval' => 'date',
                'default' => time()
            ],
        ],
        'eventstarttime' => [
            'exclude' => false,
            'label' => 'LLL:EXT:iwansoncalendar/Resources/Private/Language/locallang_db.xlf:tx_iwansoncalendar_domain_model_event.eventstarttime',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'size' => 10,
                'eval' => 'time',
                'default' => NULL
            ],
        ],        
        'eventenddate' => [
            'exclude' => false,
            'label' => 'LLL:EXT:iwansoncalendar/Resources/Private/Language/locallang_db.xlf:tx_iwansoncalendar_domain_model_event.eventenddate',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'size' => 10,
                'eval' => 'date',
                'default' => NULL
            ],
        ],
        'eventendtime' => [
            'exclude' => false,
            'label' => 'LLL:EXT:iwansoncalendar/Resources/Private/Language/locallang_db.xlf:tx_iwansoncalendar_domain_model_event.eventendtime',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'size' => 10,
                'eval' => 'time',
                'default' => NULL
            ],
        ],
        'longdescription' => [
            'exclude' => true,
            'label' => 'LLL:EXT:iwansoncalendar/Resources/Private/Language/locallang_db.xlf:tx_iwansoncalendar_domain_model_event.longdescription',
            'config' => [
                'type' => 'text',
                'enableRichtext' => true,
                'richtextConfiguration' => 'default',
                'fieldControl' => [
                    'fullScreenRichtext' => [
                        'disabled' => false,
                    ],
                ],
                'cols' => 40,
                'rows' => 15,
                'eval' => 'trim',
            ],
            
        ],
        'images' => [
            'exclude' => true,
            'label' => 'LLL:EXT:iwansoncalendar/Resources/Private/Language/locallang_db.xlf:tx_iwansoncalendar_domain_model_event.images',
            'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig(
                'images',
                [
                    'appearance' => [
                        'createNewRelationLinkTitle' => 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:media.addFileReference'
                    ],
                    'foreign_types' => [
                        '0' => [
                            'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_TEXT => [
                            'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_IMAGE => [
                            'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_AUDIO => [
                            'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_VIDEO => [
                            'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_APPLICATION => [
                            'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                        ]
                    ],
                    'maxitems' => 1,
                    'overrideChildTca' => [
                        'columns' => [
                            'crop' => [
                                
                                
                                'config' => [
                                    'type' => 'imageManipulation',
                                    'cropVariants' => [
                                        'default' => [
                                            'title' => 'Desktop',
                                            'allowedAspectRatios' => [
                                                '3:7' => [
                                                    'title' => 'Kalender-Hoch-Format (3:7)',
                                                    'value' => 3/7
                                                ],
                                            ],
                                        ],
                                        'tablet' => [
                                            'title' => 'Tablet',
                                            'allowedAspectRatios' => [
                                                '7:16' => [
                                                    'title' => 'Kalender-Hoch-Format (7:16)',
                                                    'value' => 7/16
                                                ],
                                            ],
                                        ],
                                    ]
                                ]                                

                            ],
                        ],
                    ],                    
                ],
            ),
        ],
        'place' => [
            'exclude' => true,
            'label' => 'LLL:EXT:iwansoncalendar/Resources/Private/Language/locallang_db.xlf:tx_iwansoncalendar_domain_model_event.place',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectMultipleSideBySide',
                'default' => 0,
                'maxitems' => 1,
                'multiple' => 0,
                'foreign_table' => 'tx_iwansoncalendar_domain_model_place',
                'foreign_table_where' => 'AND {#tx_iwansoncalendar_domain_model_place}.{#sys_language_uid} = ###REC_FIELD_sys_language_uid###',
                'fieldControl' => [
                    'editPopup' => [
                        'disabled' => false,
                    ],
                    'addRecord' => [
                        'disabled' => false,
                    ],
                    'listModule' => [
                        'disabled' => true,
                    ],
                ],

            ],
        ],
        'category' => [
            'exclude' => true,
            'label' => 'LLL:EXT:iwansoncalendar/Resources/Private/Language/locallang_db.xlf:tx_iwansoncalendar_domain_model_event.category',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectMultipleSideBySide',
                'default' => 0,
                'maxitems' => 1,
                'minitems' => 1,
                'multiple' => 0,
                'foreign_table' => 'tx_iwansoncalendar_domain_model_category',
                'foreign_table_where' => 'AND {#tx_iwansoncalendar_domain_model_category}.{#sys_language_uid} = ###REC_FIELD_sys_language_uid###',
                'fieldControl' => [
                    'editPopup' => [
                        'disabled' => false,
                    ],
                    'addRecord' => [
                        'disabled' => false,
                    ],
                    'listModule' => [
                        'disabled' => true,
                    ],
                ],
                
            ],
        ],
    ],
    
    

];
