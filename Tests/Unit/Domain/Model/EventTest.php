<?php
namespace Dasoe\Iwansoncalendar\Tests\Unit\Domain\Model;

/**
 * Test case.
 *
 * @author das oe <christian.oettinger@gmx.de>
 */
class EventTest extends \TYPO3\TestingFramework\Core\Unit\UnitTestCase
{
    /**
     * @var \Dasoe\Iwansoncalendar\Domain\Model\Event
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = new \Dasoe\Iwansoncalendar\Domain\Model\Event();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function getTitleReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getTitle()
        );
    }

    /**
     * @test
     */
    public function setTitleForStringSetsTitle()
    {
        $this->subject->setTitle('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'title',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getSubtitleReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getSubtitle()
        );
    }

    /**
     * @test
     */
    public function setSubtitleForStringSetsSubtitle()
    {
        $this->subject->setSubtitle('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'subtitle',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getDescriptionReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getDescription()
        );
    }

    /**
     * @test
     */
    public function setDescriptionForStringSetsDescription()
    {
        $this->subject->setDescription('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'description',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getLinkoverrideReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getLinkoverride()
        );
    }

    /**
     * @test
     */
    public function setLinkoverrideForStringSetsLinkoverride()
    {
        $this->subject->setLinkoverride('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'linkoverride',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getEventstartdateReturnsInitialValueForDateTime()
    {
        self::assertEquals(
            null,
            $this->subject->getEventstartdate()
        );
    }

    /**
     * @test
     */
    public function setEventstartdateForDateTimeSetsEventstartdate()
    {
        $dateTimeFixture = new \DateTime();
        $this->subject->setEventstartdate($dateTimeFixture);

        self::assertAttributeEquals(
            $dateTimeFixture,
            'eventstartdate',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getEventenddateReturnsInitialValueForDateTime()
    {
        self::assertEquals(
            null,
            $this->subject->getEventenddate()
        );
    }

    /**
     * @test
     */
    public function setEventenddateForDateTimeSetsEventenddate()
    {
        $dateTimeFixture = new \DateTime();
        $this->subject->setEventenddate($dateTimeFixture);

        self::assertAttributeEquals(
            $dateTimeFixture,
            'eventenddate',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getEventendtimeReturnsInitialValueForDateTime()
    {
        self::assertEquals(
            null,
            $this->subject->getEventendtime()
        );
    }

    /**
     * @test
     */
    public function setEventendtimeForDateTimeSetsEventendtime()
    {
        $dateTimeFixture = new \DateTime();
        $this->subject->setEventendtime($dateTimeFixture);

        self::assertAttributeEquals(
            $dateTimeFixture,
            'eventendtime',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getLongdescriptionReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getLongdescription()
        );
    }

    /**
     * @test
     */
    public function setLongdescriptionForStringSetsLongdescription()
    {
        $this->subject->setLongdescription('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'longdescription',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getImagesReturnsInitialValueForFileReference()
    {
        self::assertEquals(
            null,
            $this->subject->getImages()
        );
    }

    /**
     * @test
     */
    public function setImagesForFileReferenceSetsImages()
    {
        $fileReferenceFixture = new \TYPO3\CMS\Extbase\Domain\Model\FileReference();
        $this->subject->setImages($fileReferenceFixture);

        self::assertAttributeEquals(
            $fileReferenceFixture,
            'images',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getPlaceReturnsInitialValueForPlace()
    {
        self::assertEquals(
            null,
            $this->subject->getPlace()
        );
    }

    /**
     * @test
     */
    public function setPlaceForPlaceSetsPlace()
    {
        $placeFixture = new \Dasoe\Iwansoncalendar\Domain\Model\Place();
        $this->subject->setPlace($placeFixture);

        self::assertAttributeEquals(
            $placeFixture,
            'place',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getCategoryReturnsInitialValueForCategory()
    {
        self::assertEquals(
            null,
            $this->subject->getCategory()
        );
    }

    /**
     * @test
     */
    public function setCategoryForCategorySetsCategory()
    {
        $categoryFixture = new \Dasoe\Iwansoncalendar\Domain\Model\Category();
        $this->subject->setCategory($categoryFixture);

        self::assertAttributeEquals(
            $categoryFixture,
            'category',
            $this->subject
        );
    }
}
