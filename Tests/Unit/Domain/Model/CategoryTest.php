<?php
namespace Dasoe\Iwansoncalendar\Tests\Unit\Domain\Model;

/**
 * Test case.
 *
 * @author das oe <christian.oettinger@gmx.de>
 */
class CategoryTest extends \TYPO3\TestingFramework\Core\Unit\UnitTestCase
{
    /**
     * @var \Dasoe\Iwansoncalendar\Domain\Model\Category
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = new \Dasoe\Iwansoncalendar\Domain\Model\Category();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function getTitleReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getTitle()
        );
    }

    /**
     * @test
     */
    public function setTitleForStringSetsTitle()
    {
        $this->subject->setTitle('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'title',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getDescriptionReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getDescription()
        );
    }

    /**
     * @test
     */
    public function setDescriptionForStringSetsDescription()
    {
        $this->subject->setDescription('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'description',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getSymbolReturnsInitialValueForFileReference()
    {
        self::assertEquals(
            null,
            $this->subject->getSymbol()
        );
    }

    /**
     * @test
     */
    public function setSymbolForFileReferenceSetsSymbol()
    {
        $fileReferenceFixture = new \TYPO3\CMS\Extbase\Domain\Model\FileReference();
        $this->subject->setSymbol($fileReferenceFixture);

        self::assertAttributeEquals(
            $fileReferenceFixture,
            'symbol',
            $this->subject
        );
    }
}
