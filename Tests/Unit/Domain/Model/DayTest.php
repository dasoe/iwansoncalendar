<?php
namespace Dasoe\Iwansoncalendar\Tests\Unit\Domain\Model;

/**
 * Test case.
 *
 * @author das oe <christian.oettinger@gmx.de>
 */
class DayTest extends \TYPO3\TestingFramework\Core\Unit\UnitTestCase
{
    /**
     * @var \Dasoe\Iwansoncalendar\Domain\Model\Day
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = new \Dasoe\Iwansoncalendar\Domain\Model\Day();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function getUnusedReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getUnused()
        );
    }

    /**
     * @test
     */
    public function setUnusedForStringSetsUnused()
    {
        $this->subject->setUnused('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'unused',
            $this->subject
        );
    }
}
